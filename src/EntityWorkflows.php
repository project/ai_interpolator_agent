<?php

namespace Drupal\ai_interpolator_agent;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class EntityWorkflows {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * EntityWorkflows constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfo $entityTypeBundleInfo,
    EntityFieldManagerInterface $entityFieldManager,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Get all content entity bundles.
   *
   * @param bool $excludeAi
   *   Whether to exclude AI agent entity types.
   * @param bool $onlyWorkflows
   *   Whether to only include entity types that have workflows.
   *
   * @return array
   *   An array of content entity bundles.
   */
  public function getAllContentEntityBundlesOptions($excludeAi = TRUE, $onlyWorkflows = TRUE) {
    $bundles = [];
    $entityTypeDefinitions = $this->entityTypeManager->getDefinitions();
    foreach ($entityTypeDefinitions as $entityType => $entityTypeDefinition) {
      if ($entityTypeDefinition->getGroup() === 'content') {
        if ($excludeAi && in_array($entityType, [
          'ai_message',
          'ai_task',
        ])) {
          continue;
        }
        // Get each bundle for the entity type.
        $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($entityType);
        foreach ($bundleInfo as $bundle => $bundleDefinition) {
          $found = TRUE;
          if ($onlyWorkflows) {
            $found = FALSE;
            // Get the field definitions for the entity and bundle.
            $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entityType, $bundle);
            // Check third party settings for ai interpolator.
            foreach ($fieldDefinitions as $fieldDefinition) {
              if ($fieldDefinition->getConfig($bundle)->getThirdPartySetting('ai_interpolator', 'interpolator_enabled')) {
                $found = TRUE;
                break;
              }
            }
          }
          if ($found) {
            $bundles[$entityType . '--' . $bundle] = $entityTypeDefinition->getLabel() . ': ' . $bundleDefinition['label'];
          }

        }
      }
    }
    return $bundles;
  }

}
