<?php

namespace Drupal\ai_interpolator_agent\Controller;

use Drupal\ai_interpolator_agent\EntityWorkflows;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity workflows.
   *
   * @var \Drupal\ai_interpolator_agent\EntityWorkflows
   */
  protected $entityWorkflows;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityWorkflows $entityWorkflows) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityWorkflows = $entityWorkflows;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_agent.entity_workflows')
    );
  }

  /**
   * Handler for autocomplete workflows.
   */
  public function workflows(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists and longer than 3 chars.
    if (!$input || strlen($input) < 3) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    $allWorkflows = $this->entityWorkflows->getAllContentEntityBundlesOptions();
    $found = 0;
    foreach ($allWorkflows as $key => $workflow) {
      if (strpos(strtolower($workflow), strtolower($input)) !== FALSE) {
        $found++;
        $results[] = [
          'value' => $key,
          'label' => $workflow,
        ];
      }
      if ($found >= 10) {
        break;
      }
    }

    return new JsonResponse($results);
  }

  /**
   * Handler for autocomplete user e-mail request.
   */
  public function userEmail(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists and longer than 3 chars.
    if (!$input || strlen($input) < 3) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    $storage = $this->entityTypeManager()->getStorage('user');
    $query = $storage->getQuery()
      ->condition('name', $input, 'CONTAINS')
      ->condition('mail', '', '<>')
      ->sort('created', 'DESC')
      ->accessCheck(TRUE)
      ->range(0, 10);

    $ids = $query->execute();
    $users = $ids ? $storage->loadMultiple($ids) : [];

    foreach ($users as $user) {
      switch ($user->status->value) {
        case TRUE:
          $availability = '✅';
          break;

        case FALSE:
        default:
          $availability = '🚫';
          break;
      }

      $label = [
        $user->label(),
        '<small>(' . $user->getEmail() . ')</small>',
        $availability,
      ];

      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$user]),
        'label' => implode(' ', $label),
      ];
    }

    return new JsonResponse($results);
  }
}
