<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an ai approval agent entity type.
 */
interface AiApprovalAgentInterface extends ConfigEntityInterface {

}
