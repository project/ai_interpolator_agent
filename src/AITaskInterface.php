<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an ai task entity type.
 */
interface AITaskInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
