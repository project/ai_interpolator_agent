<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ai_interpolator_agent\Entity\AiWorkerAgent;

/**
 * AI Worker Agent form.
 */
final class AiWorkerAgentForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $formState): array {
    $form = parent::form($form, $formState);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Internal Name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('jason_video_watcher'),
      ],
      '#description' => $this->t('This is an unique internal name for the AI Worker Agent. It has to be unique over all the agents.'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [AiWorkerAgent::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#required' => TRUE,
      '#description' => $this->t('This is a short description what this agent does. Please be very specific and fit it into 50 or less words.'),
      '#attributes' => [
        'rows' => 2,
        'placeholder' => $this->t('This agent watcher a video and describes it either in general manner or looks for what they are prompted to look at.'),
      ],
    ];

    $workflow = $formState->getValue('workflow') ?? $this->entity->get('workflow');

    $form['workflow'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Workflow'),
      '#default_value' => $workflow,
      '#required' => TRUE,
      '#description' => $this->t('This is the AI Interpolator workflow that will be used for this agent.'),
      '#ajax' => [
        'callback' => '::getWorkflow',
        'wrapper' => 'field-connections-wrapper',
        'event' => 'change',
      ],
      '#autocomplete_route_name' => 'ai_interpolator_agent.autocomplete.workflows',
    ];

    $form['retries'] = [
      '#type' => 'number',
      '#title' => $this->t('Retries'),
      '#default_value' => $this->entity->get('retries') ?? 5,
      '#description' => $this->t('The number of retries that the agent will do before giving up and assigning it to the manual user for automatic approvals. If set to 0 it continues forever, which can be costly.'),
    ];

    $form['approval_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Approval Type'),
      '#options' => [
        'pass' => $this->t('Always Approved'),
        'pass_unpublished' => $this->t('Always Approved, but unpublished.'),
        'manual' => $this->t('Manual Approval'),
        'auto' => $this->t('Automatic Approval by Approval Agent'),
      ],
      '#default_value' => $this->entity->get('approval_type'),
      '#required' => TRUE,
    ];

    $form['fallback_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback Manual User'),
      '#description' => $this->t('The user to be used as a fallback for doing this task, if this agent is disabled or is unable to complete the task in a satisfactory way.<br>This will be synced on e-mail. <strong>Please make sure that a user with that e-mail exists on all environments</strong>'),
      '#autocomplete_route_name' => 'ai_interpolator_agent.autocomplete.usermail',
      '#default_value' => $this->entity->get('fallback_user'),
      '#required' => TRUE,
    ];

    $form['remove_entity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Garbage Collect'),
      '#default_value' => $this->entity->get('remove_entity'),
      '#description' => $this->t('Remove the entity from the database when the agent has successfully completed the task or a task that was closed for other reasons. <strong>Obviously do not enable this for workflows where this is the end product and end storage.</strong>'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
      '#description' => $this->t('If you disable an agent that has worker tasks connected to them, they will be assigned to the fallback user.'),
    ];

    $form['field_connections'] = [
      '#type' => 'details',
      '#title' => $workflow ? $this->t('Field Connection %workflow', [
        '%workflow' => $workflow
      ]) : $this->t('Choose workflow first'),
      '#attributes' => [
        'id' => 'field-connections-wrapper',
      ],
      '#open' => $workflow,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * Ajax callback for the workflow field.
   */
  public function getWorkflow(array $form, FormStateInterface $formState): array {
    return $form['field_connections'];
  }

}
