<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ai_interpolator_agent\Entity\AiApprovalAgent;

/**
 * AI Approval Agent form.
 */
final class AiApprovalAgentForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Internal Name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('mark_coca_cola_guidelines'),
      ],
      '#description' => $this->t('This is an unique internal name for the AI Approval Agent. It has to be unique over all the agents.'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [AiApprovalAgent::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#required' => TRUE,
      '#description' => $this->t('This is a short description what this agent does. Please be very specific and fit it into 50 or less words.'),
      '#attributes' => [
        'rows' => 2,
        'placeholder' => $this->t('This agent verifies that an image is following the brand guide lines of Coca-Cola Company.'),
      ],
    ];

    $exampleInstructions = "Your task is to verify that the image is following the brand guide lines of Coca-Cola Company.\n";
    $exampleInstructions .= "Make sure that the logo is looking like one of the attached images. When verifying the logo on plain text look for the following:\n";
    $exampleInstructions .= "* No added elements to the logo.\n";
    $exampleInstructions .= "* No changes to the color of the logo.\n";
    $exampleInstructions .= "* No stretching to the logo.\n";
    $exampleInstructions .= "* No editing of the logo.\n";
    $exampleInstructions .= "* No altering of the alignment of the logo.\n";
    $exampleInstructions .= "* No changes to the color of the logo. The logo should be hex #000000 or #ffffff.\n";
    $exampleInstructions .= "Logos that are displayed on bottles or cans are allowed to break the above rules, except for the color rule.\n";
    $exampleInstructions .= "The secondary colors we use are in hex: #F40000, #FF560E, #E5813E, #F79900, #D7B85B, #B59E74\n";
    $exampleInstructions .= "If a filled background exists, it should be one of the secondary colors.\n";

    $form['instructions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instructions'),
      '#default_value' => $this->entity->get('instructions'),
      '#required' => TRUE,
      '#description' => $this->t('This is a long description what this agent does. Please be very specific and fit it into 2000 or less words.'),
      '#attributes' => [
        'rows' => 15,
        'placeholder' => $this->t($exampleInstructions),
      ],
    ];

    $form['images'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Files for approval'),
      '#default_value' => $this->entity->get('images'),
      '#required' => FALSE,
      '#description' => $this->t("This is a list of images that are used for the approval process. Please list the images that are used for the approval process. The files are separated by a new line.<br>
       You can specify it in 4 ways.<ul>
       <li>Internal file uri (example public://something.jpg)</li>
       <li>Public url (example https://www.example.com/something.jpg)</li>
       <li>Relative link on the filesystem (example /var/files/test.jpg)</li>
       <li>Relative link from Drupal core (example web/modules/custom_module/test.jpg)</li>
       </ul>
       <strong>You have to make sure that the files exists on each environment.</strong>"),
       '#attributes' => [
        'rows' => 5,
        'placeholder' => $this->t('public://something.jpg
https://www.example.com/something.jpg
/var/files/test.jpg
web/modules/custom_module/test.jpg'),
       ],
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
      '#description' => $this->t('If you disable an agent that has approval tasks connected to them, they will be assigned to the fallback user.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
