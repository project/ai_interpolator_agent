<?php

namespace Drupal\ai_interpolator_agent\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Ai Agent Form access.
 */
class AiAgentForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'ai_interpolat_agent.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_interpolat_agent_base_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['fallback_approval_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback Manual Approval User'),
      '#description' => $this->t('The user to be used as a fallback for approval, if no agents fits or is unable to complete the task in a satisfactory way.<br>This will be synced on e-mail. <strong>Please make sure that a user with that e-mail exists on all environments</strong>'),
      '#autocomplete_route_name' => 'ai_interpolator_agent.autocomplete.usermail',
      '#default_value' => $config->get('fallback_approval_user'),
      '#required' => TRUE,
    ];

    $form['fallback_worker_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback Manual Worker User'),
      '#description' => $this->t('The user to be used as a fallback for doing a task, if no agents fits or is unable to complete the task in a satisfactory way.<br>This will be synced on e-mail. <strong>Please make sure that a user with that e-mail exists on all environments</strong>'),
      '#autocomplete_route_name' => 'ai_interpolator_agent.autocomplete.usermail',
      '#default_value' => $config->get('fallback_worker_user'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('fallback_approval_user', $form_state->getValue('fallback_approval_user'))
      ->set('fallback_worker_user', $form_state->getValue('fallback_worker_user'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
