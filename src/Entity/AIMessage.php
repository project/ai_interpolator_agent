<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent\Entity;

use Drupal\ai_interpolator_agent\AIMessageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the ai message entity class.
 *
 * @ContentEntityType(
 *   id = "ai_message",
 *   label = @Translation("AI Message"),
 *   label_collection = @Translation("AI Messages"),
 *   label_singular = @Translation("ai message"),
 *   label_plural = @Translation("ai messages"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ai messages",
 *     plural = "@count ai messages",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ai_interpolator_agent\AIMessageListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\ai_interpolator_agent\AIMessageAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\ai_interpolator_agent\Form\AIMessageForm",
 *       "edit" = "Drupal\ai_interpolator_agent\Form\AIMessageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ai_message",
 *   admin_permission = "administer ai_message",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/ai-message",
 *     "add-form" = "/ai-interpolator/message/add",
 *     "canonical" = "/ai-interpolator/message/{ai_message}",
 *     "edit-form" = "/ai-interpolator/message/{ai_message}/edit",
 *     "delete-form" = "/ai-interpolator/message/{ai_message}/delete",
 *     "delete-multiple-form" = "/admin/content/ai-message/delete-multiple",
 *   },
 * )
 */
final class AIMessage extends ContentEntityBase implements AIMessageInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the ai message was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the ai message was last edited.'));

    return $fields;
  }

}
