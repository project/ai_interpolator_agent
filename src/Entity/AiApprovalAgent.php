<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent\Entity;

use Drupal\ai_interpolator_agent\AiApprovalAgentInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the ai approval agent entity type.
 *
 * @ConfigEntityType(
 *   id = "ai_approval_agent",
 *   label = @Translation("AI Approval Agent"),
 *   label_collection = @Translation("AI Approval Agents"),
 *   label_singular = @Translation("AI Approval Agent"),
 *   label_plural = @Translation("AI Approval Agents"),
 *   label_count = @PluralTranslation(
 *     singular = "@count AI Approval Agent",
 *     plural = "@count AI Approval Agents",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ai_interpolator_agent\AiApprovalAgentListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ai_interpolator_agent\Form\AiApprovalAgentForm",
 *       "edit" = "Drupal\ai_interpolator_agent\Form\AiApprovalAgentForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "ai_approval_agent",
 *   admin_permission = "administer ai_approval_agent",
 *   links = {
 *     "collection" = "/admin/structure/ai-approval-agent",
 *     "add-form" = "/admin/structure/ai-approval-agent/add",
 *     "edit-form" = "/admin/structure/ai-approval-agent/{ai_approval_agent}",
 *     "delete-form" = "/admin/structure/ai-approval-agent/{ai_approval_agent}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 * )
 */
final class AiApprovalAgent extends ConfigEntityBase implements AiApprovalAgentInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $label;

  /**
   * The example description.
   */
  protected string $description;

}
