<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent\Entity;

use Drupal\ai_interpolator_agent\AiWorkerAgentInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the ai worker agent entity type.
 *
 * @ConfigEntityType(
 *   id = "ai_worker_agent",
 *   label = @Translation("AI Worker Agent"),
 *   label_collection = @Translation("AI Worker Agents"),
 *   label_singular = @Translation("ai worker agent"),
 *   label_plural = @Translation("ai worker agents"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ai worker agent",
 *     plural = "@count ai worker agents",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ai_interpolator_agent\AiWorkerAgentListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ai_interpolator_agent\Form\AiWorkerAgentForm",
 *       "edit" = "Drupal\ai_interpolator_agent\Form\AiWorkerAgentForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "ai_worker_agent",
 *   admin_permission = "administer ai_worker_agent",
 *   links = {
 *     "collection" = "/admin/structure/ai-worker-agent",
 *     "add-form" = "/admin/structure/ai-worker-agent/add",
 *     "edit-form" = "/admin/structure/ai-worker-agent/{ai_worker_agent}",
 *     "delete-form" = "/admin/structure/ai-worker-agent/{ai_worker_agent}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 * )
 */
final class AiWorkerAgent extends ConfigEntityBase implements AiWorkerAgentInterface {

  /**
   * The ID.
   */
  protected string $id;

  /**
   * The label.
   */
  protected string $label;

  /**
   * The description.
   */
  protected string $description;

  /**
   * The workflow
   */
  protected string $workflow;

}
