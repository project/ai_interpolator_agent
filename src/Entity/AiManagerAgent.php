<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent\Entity;

use Drupal\ai_interpolator_agent\AiManagerAgentInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the ai manager agent entity type.
 *
 * @ConfigEntityType(
 *   id = "ai_manager_agent",
 *   label = @Translation("AI Manager Agent"),
 *   label_collection = @Translation("AI Manager Agents"),
 *   label_singular = @Translation("ai manager agent"),
 *   label_plural = @Translation("ai manager agents"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ai manager agent",
 *     plural = "@count ai manager agents",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ai_interpolator_agent\AiManagerAgentListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ai_interpolator_agent\Form\AiManagerAgentForm",
 *       "edit" = "Drupal\ai_interpolator_agent\Form\AiManagerAgentForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "ai_manager_agent",
 *   admin_permission = "administer ai_manager_agent",
 *   links = {
 *     "collection" = "/admin/structure/ai-manager-agent",
 *     "add-form" = "/admin/structure/ai-manager-agent/add",
 *     "edit-form" = "/admin/structure/ai-manager-agent/{ai_manager_agent}",
 *     "delete-form" = "/admin/structure/ai-manager-agent/{ai_manager_agent}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 * )
 */
final class AiManagerAgent extends ConfigEntityBase implements AiManagerAgentInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $label;

  /**
   * The example description.
   */
  protected string $description;

}
