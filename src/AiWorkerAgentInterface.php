<?php declare(strict_types = 1);

namespace Drupal\ai_interpolator_agent;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an ai worker agent entity type.
 */
interface AiWorkerAgentInterface extends ConfigEntityInterface {

}
